Name:            libmikmod
Version:         3.3.12
Release:         1
Summary:         MOD Music File Player library
License:         GPL-2.0-only AND LGPL-2.1-or-later
URL:             https://mikmod.sourceforge.net/
Source0:         https://downloads.sourceforge.net/mikmod/libmikmod-%{version}.tar.gz
Patch0001:       libmikmod-64bit.patch
Patch0002:       libmikmod-multilib.patch
BuildRequires:   alsa-lib-devel pulseaudio-libs-devel gcc
Suggests:        alsa-lib
Suggests:        pulseaudio-libs

%description
libmikmod is a library of mikmod MOD music file players for systems similar to
UNIX. Supported file formats include: MOD, STM, S3M, MTM, XM, ULT, and IT.

%package         devel
Summary:         Header files and documentation for compiling mikmod applications
Provides:        mikmod-devel = %{version}-%{release}
Requires:        %{name} = %{version}-%{release} pulseaudio-libs-devel

%description     devel
The libmikmod-devel package includes header files and libraries necessary
for the libmikmod library.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --enable-dl --enable-alsa --disable-simd --disable-static
%make_build

%install
%make_install
%delete_la

%files
%license COPYING.LIB COPYING.LESSER
%{_libdir}/libmikmod.so.3*

%files devel
%{_bindir}/%{name}-config
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/aclocal/%{name}.m4
%{_includedir}/mikmod.h
%{_infodir}/mikmod.info*

%files help
%doc AUTHORS NEWS README TODO
%{_mandir}/man1/%{name}-config*

%changelog
* Wed Jan 01 2025 Funda Wang <fundawang@yeah.net> - 3.3.12-1
- update to 3.3.12

* Tue Jun 08 2021 wulei <wulei80@huawei.com> - 3.3.11.1-7
- fixes failed: error: no acceptable C compiler found in $PATH

* Sun Sep 20 2020 yanan li <liyanan032@huawei.com> - 3.3.11.1-6
- delete %postun

* Wed Nov 27 2019 gulining<gulining1@huawei.com> - 3.3.11.1-5
- Pakcage init
